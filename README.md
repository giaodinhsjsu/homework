# Convert Hex to Binary 
Write a function to_binary that takes a number,
and return the string equivalent version of the number in binary

## Installation

Use [python] (https://www.python.org/downloads/) to for this program.

## Usage

```python
number = int(input())

# returns 'Binary: 0b '
return "Binary: 0b" + answer

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

